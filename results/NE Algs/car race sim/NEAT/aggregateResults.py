import os

path = os.getcwd()

files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

tot = 0.0
totCounter = 0

for fname in files:
	if fname == "aggregateResults.py":
		continue
	else:
		f = open(fname)
		lines = f.readlines()
		f.close()

		correctedLines = lines[:2000]

		outFile = open("aggregatedResults", "a+")
		for correctedLine in correctedLines:
			outFile.write(correctedLine)

		tot += float(lines[2000].split(":")[1])
		totCounter += 1

		outFile.close()

timerFile = open("AverageTime", "a+")
timerFile.write(str(tot / totCounter))
timerFile.close();