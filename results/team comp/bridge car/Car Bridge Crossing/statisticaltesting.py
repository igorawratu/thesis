import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats

path = os.getcwd()

files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
files.remove("statisticaltesting.py")

datList = []
meanList = []
outTests = []
for f in files:
	outTests.append([])
lineStyles = ["-","--","-.",":"]
lineColours = ["blue", "green", "black", "red"]
linewidths = [4, 4, 4, 4, 1, 1, 1, 1, 3, 3, 3, 3, 2, 2, 2, 2]

for fname in files:
	meanFileName = fname + "_means"

	dat = np.loadtxt(fname)
	dat.shape = (30, 2000)
	dat = dat.T

	meanDat = np.mean(dat, axis = 1)

	np.savetxt(meanFileName, meanDat)

	datList.append(dat)
	meanList.append(meanDat)

iter = np.nditer(meanList[0], flags=['f_index'])

while not iter.finished:
	currIndex = iter.index
	lowestMeanPos = 0
	lowestMeanVal = meanList[0][currIndex]

	for idx, val in enumerate(meanList):
		if val[currIndex] < lowestMeanVal:
			lowestMeanPos = idx
			lowestMeanVal = val[currIndex]

	for idx, dat in enumerate(datList):
		u, p = spstats.mannwhitneyu(dat[iter.index], datList[lowestMeanPos][iter.index])
		sigma = np.std(dat[iter.index])
		outTests[idx].append("U-Value: " + str(u) + " P-Value: " + str(p) + " sigma: " + str(sigma))

	iter.iternext()

for idx, means in enumerate(meanList):
	plt.plot(means, label = files[idx], linestyle = lineStyles[idx], color = lineColours[idx], linewidth = linewidths[idx])

for idx, tests in enumerate(outTests):
	fname = files[idx] + "_stats"

	f = open(fname, "w+")
	for line in tests:
		f.write(line + "\n")
	f.close()

labels = [l * 5000 for l in range(5)]

locs, oldLbls = plt.xticks()
plt.xticks(locs, labels)

plt.ylabel("Fitness")
plt.xlabel("Fitness Evaluations")
plt.legend()
currDir = os.path.dirname(os.path.realpath(__file__))
currFolderName = currDir.split("\\")[-1]
plt.title(currFolderName)
plt.show()


